## Standard Nodejs Config

findPaths grabs the tsconfig.json from the root of the project and grabs all of the path alias that are listed there.
Those alias properties are passed to webpack so that the runtime has access to those paths.

### tsconfig.json

```json
{
    "compilerOptions": {
        "module": "commonjs",
        "target": "es2017",
        "lib": ["es6", "dom"],
        "moduleResolution": "node",
        "outDir": "./dist",
        "baseUrl": "src",
        "paths": {
            "@date/*": ["date-util/*"]
        },
        "sourceMap": true,
        "types": ["node", "mocha"],
        "typeRoots": ["node_modules/@types", "src/types"]
    },
    "exclude": ["node_modules"]
}
```

### webpack.config.js

```typescript
const { findPaths, buildTypeScriptConfig } = require("./webpack-utils");

const alias = findPaths();

// Runner is optional
const entry = {
    app: "./src/app/app.ts",
    runner: "./src/runner.ts",
};

const exportConfig = buildTypeScriptConfig()
    .entryPoints(entry)
    .aliasMap(alias)
    .clearTerminal();

module.exports = exportConfig.build();
```

## Graphql Nodejs Config

    Note the addModuleRule for the graphql-tag/loader

### webpack.config.js

```typescript
const {
    findPaths,
    runnerExists,
    buildTypeScriptConfig,
    listSrc,
} = require("./webpack-utils");

const alias = findPaths();

// Runner is optional
const entry = {
    app: "./src/app/app.ts",
    runner: "./src/runner.ts",
};

const exportConfig = buildTypeScriptConfig()
    .entryPoints(entry)
    .aliasMap(alias)
    .clearTerminal()
    .addModuleRule({
        test: /\.graphql$/,
        exclude: /node_modules/,
        loader: "graphql-tag/loader",
    });

module.exports = exportConfig.build();
```

## Library Config

    Note the isLibrary() and removeSourceMap() calls.

This will make the generated webpack file be configured to be injected. The webpack-utils project is built on this
concept. This can be seen in the webpack.dev.js file.

### webpack.config.js

```typescript
const { buildTypeScriptConfig, findPaths } = require("./webpack-utils");

const entry = {
    "webpack-utils": "./src/webpack-utils.ts",
};

const aliasMap = findPaths();

const exportConfig = buildTypeScriptConfig()
    .entryPoints(entry)
    .aliasMap(aliasMap)
    .isLibrary()
    .removeSourceMap();

module.exports = exportConfig.build();
```

## List source entry points and Runner concept

One style of arranging entry points is as top level entries in the source folder.

    -- src
       - app
       - logic
       - tool-box

listSrc allows for the config to grab all top level files in the folder to be listed as webpack entry points.

runnerExists also checks for a file called runner.ts at the root level of the "src" folder. This can be used to remove
the try from the entryMap or added to it based on the style of the project.

### webpack.config.js

```typescript
const {
    findPaths,
    runnerExists,
    buildTypeScriptConfig,
    listSrc,
} = require("./webpack-utils");

const alias = findPaths();

const addToolbox = false;

const toolbox = addToolbox ? listSrc("tool-box") : {};

const entry = {
    app: "./src/app/app.ts",
    runner: "./src/runner.ts",
    ...toolbox,
};

if (!runnerExists()) {
    delete entry["runner"];
}

const exportConfig = buildTypeScriptConfig()
    .entryPoints(entry)
    .aliasMap(alias)
    .clearTerminal();

module.exports = exportConfig.build();
```

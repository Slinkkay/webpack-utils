import { TypescriptConfigBuilder } from "@domain/config";
import { directoryPath } from "@infrastructure/fs";
import { getTsConfigPath } from "@domain/config";
import fs = require("fs");
export * from "@domain/find-paths";

export function listSrc(entries: string) {
    const consoleDir = directoryPath(`/src/${entries}`);

    const result = fs.readdirSync(consoleDir);

    const entryMap = {};

    result.forEach((item) => {
        if (item.endsWith(".ts")) {
            item = item.substr(0, item.length - 3);
        }
        entryMap[item] = directoryPath(`/src/${entries}/${item}`);
    });

    return entryMap;
}

export function runnerExists() {
    return fs.existsSync(directoryPath("/src/runner.ts"));
}

export function buildTypeScriptConfig(): TypescriptConfigBuilder {
    return new TypescriptConfigBuilder();
}

function getTsConfig() {
    return JSON.parse(fs.readFileSync(getTsConfigPath()).toString());
}

function addDeclarationToConfig(config) {
    config.compilerOptions.declaration = true;
    return config;
}

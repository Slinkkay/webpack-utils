import { directoryPath } from "@infrastructure/fs";

export type Environment = "production" | "development";

export class TypescriptConfigBuilder {
    private library = false;
    private ttsc = false;
    private includeSourceMap = true;
    private shoudClearTerminal = false;
    private production: boolean;
    private alias: any = {};
    private entry: any = {};
    private modulesRules = [];

    isLibrary(): TypescriptConfigBuilder {
        this.library = true;
        return this;
    }

    useTTSC(): TypescriptConfigBuilder {
        this.ttsc = true;
        return this;
    }

    entryPoints(entry: any): TypescriptConfigBuilder {
        this.entry = entry;
        return this;
    }

    isProduction(): TypescriptConfigBuilder {
        this.production = true;
        return this;
    }

    aliasMap(alias: any): TypescriptConfigBuilder {
        this.alias = alias;
        return this;
    }

    removeSourceMap(): TypescriptConfigBuilder {
        this.includeSourceMap = false;
        return this;
    }

    clearTerminal(): TypescriptConfigBuilder {
        this.shoudClearTerminal = true;
        return this;
    }

    addModuleRule(moduleRule): TypescriptConfigBuilder {
        this.modulesRules.push(moduleRule);
        return this;
    }

    build(): any {
        const environment = this.production ? "production" : "development";
        let base = baseTypescriptConfig(this.entry, environment, this.alias);
        if (this.ttsc) {
            base.module.rules.push(buildTtscLoaderRule());
        } else {
            base.module.rules.push(buildTsLoaderRule());
        }
        base.module.rules.push(...this.modulesRules);

        if (this.library) {
            base = addLibraryTarget(base);
        }
        if (!this.includeSourceMap) {
            base.devtool = "cheap-source-map";
        }
        if (this.shoudClearTerminal) {
            const CleanTerminalPlugin = require("clean-terminal-webpack-plugin");
            base.plugins.push(new CleanTerminalPlugin({ beforeCompile: true }));
        }

        return base;
    }
}

function addLibraryTarget(config) {
    return {
        ...config,
        output: {
            ...config.output,
            libraryTarget: "commonjs",
        },
    };
}

function buildTsLoaderRule(): any {
    return {
        test: /\.ts$/,
        loader: "ts-loader",
        options: {
            configFile: "tsconfig.json",
        },
    };
}

function buildTtscLoaderRule(): any {
    return {
        test: /\.ts$/,
        loader: "ts-loader",
        options: {
            configFile: "tsconfig.json",
            compiler: "ttypescript",
        },
    };
}

function baseTypescriptConfig(entry: any, env: Environment, alias: any) {
    const nodeExternals = require("webpack-node-externals");
    return {
        entry: entry,
        mode: env,
        target: "node",
        output: {
            path: directoryPath("dist"),
            filename: "[name].js",
            devtoolModuleFilenameTemplate: "[absolute-resource-path]",
            devtoolFallbackModuleFilenameTemplate:
                "[absolute-resource-path]?[hash]",
        },
        plugins: [],
        devtool: "inline-cheap-module-source-map",
        resolve: {
            extensions: [".ts", ".js"],
            alias: alias,
        },
        externals: [nodeExternals()],
        module: {
            rules: [],
        },
    };
}

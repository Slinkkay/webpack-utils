import path = require("path");
import fs = require("fs");
export * from "./builder";

function joinCurrent(target: string) {
    return path.join(process.cwd(), target);
}

export function getTsConfigPath() {
    return joinCurrent("tsconfig.json");
}

function getTsConfig() {
    return JSON.parse(fs.readFileSync(getTsConfigPath()).toString());
}

import fs = require("fs");
import { getTsConfigPath } from "@domain/config";
import { directoryPath } from "@infrastructure/fs";

/**
 * Finds all paths configured in tsconfig.json and adds them as alias for webpack
 */
export function findPaths(sourceRoot = "src") {
    const tsConfig = JSON.parse(fs.readFileSync(getTsConfigPath()).toString());

    let alias = {};
    let pathRef = tsConfig.compilerOptions.paths;
    for (let property in pathRef) {
        if (pathRef.hasOwnProperty(property)) {
            // Get the name of the alias and it's reference Multiple references aren't supported
            let aliasName = property;
            let aliasReference = tsConfig.compilerOptions.paths[property][0];
            // If alias ends with a wildcard then webpack item needs it removed for both items
            if (property.endsWith("/*")) {
                aliasName = property.substr(0, property.length - 2);
                aliasReference = aliasReference.substr(
                    0,
                    aliasReference.length - 2,
                );
            }
            alias[aliasName] = directoryPath([sourceRoot, aliasReference]);
        }
    }
    return alias;
}

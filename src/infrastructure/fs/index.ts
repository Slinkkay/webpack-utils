import path = require("path");
import * as process from "process";

export function directoryPath(structure: string[] | string): string {
    if (!Array.isArray(structure)) {
        return path.join(process.cwd(), structure);
    }
    if (structure.length === 0) {
        return process.cwd();
    }
    const copy = [...structure];
    const target = copy.pop();
    return path.join(directoryPath(copy), target);
}

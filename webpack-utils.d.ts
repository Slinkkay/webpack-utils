// Generated by dts-bundle-generator v8.0.1

declare class TypescriptConfigBuilder {
	private library;
	private ttsc;
	private includeSourceMap;
	private shoudClearTerminal;
	private production;
	private alias;
	private entry;
	private modulesRules;
	isLibrary(): TypescriptConfigBuilder;
	useTTSC(): TypescriptConfigBuilder;
	entryPoints(entry: any): TypescriptConfigBuilder;
	isProduction(): TypescriptConfigBuilder;
	aliasMap(alias: any): TypescriptConfigBuilder;
	removeSourceMap(): TypescriptConfigBuilder;
	clearTerminal(): TypescriptConfigBuilder;
	addModuleRule(moduleRule: any): TypescriptConfigBuilder;
	build(): any;
}
/**
 * Finds all paths configured in tsconfig.json and adds them as alias for webpack
 */
export declare function findPaths(sourceRoot?: string): {};
export declare function listSrc(entries: string): {};
export declare function runnerExists(): boolean;
export declare function buildTypeScriptConfig(): TypescriptConfigBuilder;

export {};

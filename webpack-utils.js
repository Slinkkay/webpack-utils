/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app/webpack-utils.ts":
/*!**********************************!*\
  !*** ./src/app/webpack-utils.ts ***!
  \**********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.buildTypeScriptConfig = exports.runnerExists = exports.listSrc = void 0;
const config_1 = __webpack_require__(/*! @domain/config */ "./src/domain/config/index.ts");
const fs_1 = __webpack_require__(/*! @infrastructure/fs */ "./src/infrastructure/fs/index.ts");
const config_2 = __webpack_require__(/*! @domain/config */ "./src/domain/config/index.ts");
const fs = __webpack_require__(/*! fs */ "fs");
__exportStar(__webpack_require__(/*! @domain/find-paths */ "./src/domain/find-paths/index.ts"), exports);
function listSrc(entries) {
    const consoleDir = (0, fs_1.directoryPath)(`/src/${entries}`);
    const result = fs.readdirSync(consoleDir);
    const entryMap = {};
    result.forEach((item) => {
        if (item.endsWith(".ts")) {
            item = item.substr(0, item.length - 3);
        }
        entryMap[item] = (0, fs_1.directoryPath)(`/src/${entries}/${item}`);
    });
    return entryMap;
}
exports.listSrc = listSrc;
function runnerExists() {
    return fs.existsSync((0, fs_1.directoryPath)("/src/runner.ts"));
}
exports.runnerExists = runnerExists;
function buildTypeScriptConfig() {
    return new config_1.TypescriptConfigBuilder();
}
exports.buildTypeScriptConfig = buildTypeScriptConfig;
function getTsConfig() {
    return JSON.parse(fs.readFileSync((0, config_2.getTsConfigPath)()).toString());
}
function addDeclarationToConfig(config) {
    config.compilerOptions.declaration = true;
    return config;
}


/***/ }),

/***/ "./src/domain/config/builder.ts":
/*!**************************************!*\
  !*** ./src/domain/config/builder.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.TypescriptConfigBuilder = void 0;
const fs_1 = __webpack_require__(/*! @infrastructure/fs */ "./src/infrastructure/fs/index.ts");
class TypescriptConfigBuilder {
    constructor() {
        this.library = false;
        this.ttsc = false;
        this.includeSourceMap = true;
        this.shoudClearTerminal = false;
        this.alias = {};
        this.entry = {};
        this.modulesRules = [];
    }
    isLibrary() {
        this.library = true;
        return this;
    }
    useTTSC() {
        this.ttsc = true;
        return this;
    }
    entryPoints(entry) {
        this.entry = entry;
        return this;
    }
    isProduction() {
        this.production = true;
        return this;
    }
    aliasMap(alias) {
        this.alias = alias;
        return this;
    }
    removeSourceMap() {
        this.includeSourceMap = false;
        return this;
    }
    clearTerminal() {
        this.shoudClearTerminal = true;
        return this;
    }
    addModuleRule(moduleRule) {
        this.modulesRules.push(moduleRule);
        return this;
    }
    build() {
        const environment = this.production ? "production" : "development";
        let base = baseTypescriptConfig(this.entry, environment, this.alias);
        if (this.ttsc) {
            base.module.rules.push(buildTtscLoaderRule());
        }
        else {
            base.module.rules.push(buildTsLoaderRule());
        }
        base.module.rules.push(...this.modulesRules);
        if (this.library) {
            base = addLibraryTarget(base);
        }
        if (!this.includeSourceMap) {
            base.devtool = "cheap-source-map";
        }
        if (this.shoudClearTerminal) {
            const CleanTerminalPlugin = __webpack_require__(/*! clean-terminal-webpack-plugin */ "clean-terminal-webpack-plugin");
            base.plugins.push(new CleanTerminalPlugin({ beforeCompile: true }));
        }
        return base;
    }
}
exports.TypescriptConfigBuilder = TypescriptConfigBuilder;
function addLibraryTarget(config) {
    return Object.assign(Object.assign({}, config), { output: Object.assign(Object.assign({}, config.output), { libraryTarget: "commonjs" }) });
}
function buildTsLoaderRule() {
    return {
        test: /\.ts$/,
        loader: "ts-loader",
        options: {
            configFile: "tsconfig.json",
        },
    };
}
function buildTtscLoaderRule() {
    return {
        test: /\.ts$/,
        loader: "ts-loader",
        options: {
            configFile: "tsconfig.json",
            compiler: "ttypescript",
        },
    };
}
function baseTypescriptConfig(entry, env, alias) {
    const nodeExternals = __webpack_require__(/*! webpack-node-externals */ "webpack-node-externals");
    return {
        entry: entry,
        mode: env,
        target: "node",
        output: {
            path: (0, fs_1.directoryPath)("dist"),
            filename: "[name].js",
            devtoolModuleFilenameTemplate: "[absolute-resource-path]",
            devtoolFallbackModuleFilenameTemplate: "[absolute-resource-path]?[hash]",
        },
        plugins: [],
        devtool: "inline-cheap-module-source-map",
        resolve: {
            extensions: [".ts", ".js"],
            alias: alias,
        },
        externals: [nodeExternals()],
        module: {
            rules: [],
        },
    };
}


/***/ }),

/***/ "./src/domain/config/index.ts":
/*!************************************!*\
  !*** ./src/domain/config/index.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getTsConfigPath = void 0;
const path = __webpack_require__(/*! path */ "path");
const fs = __webpack_require__(/*! fs */ "fs");
__exportStar(__webpack_require__(/*! ./builder */ "./src/domain/config/builder.ts"), exports);
function joinCurrent(target) {
    return path.join(process.cwd(), target);
}
function getTsConfigPath() {
    return joinCurrent("tsconfig.json");
}
exports.getTsConfigPath = getTsConfigPath;
function getTsConfig() {
    return JSON.parse(fs.readFileSync(getTsConfigPath()).toString());
}


/***/ }),

/***/ "./src/domain/find-paths/index.ts":
/*!****************************************!*\
  !*** ./src/domain/find-paths/index.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.findPaths = void 0;
const fs = __webpack_require__(/*! fs */ "fs");
const config_1 = __webpack_require__(/*! @domain/config */ "./src/domain/config/index.ts");
const fs_1 = __webpack_require__(/*! @infrastructure/fs */ "./src/infrastructure/fs/index.ts");
/**
 * Finds all paths configured in tsconfig.json and adds them as alias for webpack
 */
function findPaths(sourceRoot = "src") {
    const tsConfig = JSON.parse(fs.readFileSync((0, config_1.getTsConfigPath)()).toString());
    let alias = {};
    let pathRef = tsConfig.compilerOptions.paths;
    for (let property in pathRef) {
        if (pathRef.hasOwnProperty(property)) {
            // Get the name of the alias and it's reference Multiple references aren't supported
            let aliasName = property;
            let aliasReference = tsConfig.compilerOptions.paths[property][0];
            // If alias ends with a wildcard then webpack item needs it removed for both items
            if (property.endsWith("/*")) {
                aliasName = property.substr(0, property.length - 2);
                aliasReference = aliasReference.substr(0, aliasReference.length - 2);
            }
            alias[aliasName] = (0, fs_1.directoryPath)([sourceRoot, aliasReference]);
        }
    }
    return alias;
}
exports.findPaths = findPaths;


/***/ }),

/***/ "./src/infrastructure/fs/index.ts":
/*!****************************************!*\
  !*** ./src/infrastructure/fs/index.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.directoryPath = void 0;
const path = __webpack_require__(/*! path */ "path");
const process = __webpack_require__(/*! process */ "process");
function directoryPath(structure) {
    if (!Array.isArray(structure)) {
        return path.join(process.cwd(), structure);
    }
    if (structure.length === 0) {
        return process.cwd();
    }
    const copy = [...structure];
    const target = copy.pop();
    return path.join(directoryPath(copy), target);
}
exports.directoryPath = directoryPath;


/***/ }),

/***/ "clean-terminal-webpack-plugin":
/*!************************************************!*\
  !*** external "clean-terminal-webpack-plugin" ***!
  \************************************************/
/***/ ((module) => {

module.exports = require("clean-terminal-webpack-plugin");

/***/ }),

/***/ "webpack-node-externals":
/*!*****************************************!*\
  !*** external "webpack-node-externals" ***!
  \*****************************************/
/***/ ((module) => {

module.exports = require("webpack-node-externals");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("fs");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("path");

/***/ }),

/***/ "process":
/*!**************************!*\
  !*** external "process" ***!
  \**************************/
/***/ ((module) => {

module.exports = require("process");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./src/app/webpack-utils.ts");
/******/ 	var __webpack_export_target__ = exports;
/******/ 	for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
/******/ 	if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ 	
/******/ })()
;
//# sourceMappingURL=webpack-utils.js.map
const { buildTypeScriptConfig, findPaths } = require("./webpack-utils");

const entry = {
    "webpack-utils": "./src/app/webpack-utils.ts",
};

const aliasMap = findPaths();

const exportConfig = buildTypeScriptConfig()
    .entryPoints(entry)
    .aliasMap(aliasMap)
    .isLibrary()
    .removeSourceMap();

module.exports = exportConfig;
